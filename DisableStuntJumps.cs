using GTA;
using System;

namespace DisableStuntJumps
{
    public class DisableStuntJumps : Script
    {
        public DisableStuntJumps()
        {
            GTA.Native.Function.Call("ALLOW_STUNT_JUMPS_TO_TRIGGER", false);
        }
    }
}